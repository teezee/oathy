# Oathy

`oathy` - securely store and retrieve 2FA OTP secrets on the command line

    (C) 2022 Thomas Zink <thomas.zink@uni-konstanz.de>
    Licensed under the GNU General Public License, version 2

`oathy` is a command line tool to create an encrypted store for OTP secrets 
(similar to `pass` for passwords). It uses `gpg` for encryption, and `oathtool`
to generate the otp codes.

Based on an article and code by Vivek Gite (2021) <https://www.cyberciti.biz/faq/use-oathtool-linux-command-line-for-2-step-verification-2fa/>

## Requirements & Prerequisits

Install the following packages

- **oath-toolkit** / **oathtool**: to generate the OTP codes
- **gnupg**: to encrypt & decrypt the secret store
- **xclip** (optional): to copy the OTP code to clipboard
- **dmenu** (optional): used by `oathy-dmenu` for `dmenu` integration

To encrypt the store, a gpg key is required. If you do not already have one,
generate a new key pair using:

    $ gpg --full-gen-key 

Use RSA and RSA for key type, and 4096 bits for keysize.

## Usage

    oathy [issuer:]<account>[@domain]
    
`oathy` assumes the location of the secret store is `$HOME/.pki/oathy`. The
location can be set using the envirnment variable `OATHYHOME` (put into your
shell rc file).

`oathy` takes a single argument with optional parts. At least an account name
must be provided.

- [*issuer:*] (optional): issuer of the secret, e.g. github.com
- <*account*>: name of the account / user, e.g. mygituser
- [*@domain*]: domain of the account, e.g. @github.com

The encrypted secrets are stored under `$OATHYHOME/issuer:account@domain/secret.gpg`

`oathy` checks if a stored secret exists. If it can be found,
it is decrypted and the OTP code is printed to stdout (and copied to clipboard).
If no secret can be found, `oathy` creates a new store and asks the user for
the OTP secret (in base32) and the gpg recipient key (email).

